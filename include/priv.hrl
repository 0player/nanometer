%%%-------------------------------------------------------------------
%%% @author Alex S
%%% @copyright (C) 2016, Alex S
%%% @doc
%%%
%%% @end
%%% Created : 2016-09-20 18:46
%%%-------------------------------------------------------------------
-author("alex0player@gmail.com").

-define(COUNTER_TABLE, nanometer_counter).
-define(COUNTER_TX_TABLE, nanometer_counter_tx).

-define(GAUGE_TABLE, nanometer_gauge).
-define(GAUGE_TX_TABLE, nanometer_gauge_tx).

-define(METER_TABLE, nanometer_meter).

-define(HISTOGRAM_SUPERVISOR, nanometer_histogram_sup).
-define(HISTOGRAM_TABLE, nanometer_histogram).

-define(EXTERNAL_TABLE, nanometer_external).
