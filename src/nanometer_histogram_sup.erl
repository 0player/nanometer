%%%-------------------------------------------------------------------
%%% @author Alex S
%%% @copyright (C) 2016, Alex S
%%% @doc
%%%
%%% @end
%%% Created : 2016-09-21 17:15
%%%-------------------------------------------------------------------
-module(nanometer_histogram_sup).
-author("alex0player@gmail.com").

-behaviour(supervisor).

-include("priv.hrl").
%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  supervisor:start_link({local, ?HISTOGRAM_SUPERVISOR}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([]) ->
  %% create a table
  {ok, {
    #{strategy => simple_one_for_one, intensity => 1, period => 5},
    [
      #{id => histogram_srv,
        start => {nanometer_histogram_srv, start_link, []},
        restart => temporary,
        shutdown => brutal_kill,
        type => worker}
    ]
  }}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
